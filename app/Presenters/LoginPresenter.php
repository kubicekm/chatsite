<?php

namespace App\Presenters;

use App\Model\UsersManager;
use Nette\Application\UI\Form;
use Nette\Application\UI\Presenter;
use Nette\Security\AuthenticationException;


final class LoginPresenter extends BasePresenter{
    /** @var UsersManager @inject */
    public $usersManager;

    public function createComponentSignInForm(): Form {
        $form = new Form();

        $form->addText('username', 'Username')
                ->setRequired(true);

        $form->addPassword('password', 'Password')
                ->setRequired(true);

        $form->addSubmit('submit', 'Login');

        $form->onSuccess[] = [$this, 'onSignInFormSubmit'];

        return $form;
    }


    public function onSignInFormSubmit(\stdClass $values) {
        try {
            $this->getUser()->login($values->username, $values->password);
            $this->flashMessage('Logged in successfully!.', 'success');
            $this->redirect('Homepage:');
        } catch (AuthenticationException $e) {
            $this->flashMessage($e->getMessage(), 'danger');
        }
    }

    public function actionDefault() {
        if($this->user->isLoggedIn())$this->redirect("Homepage:");
    }

    public function actionOut() {
        $this->getUser()->logout(true);
        $this->flashMessage('Logged off successfuly!.', 'success');
        $this->redirect('Homepage:');
    } 
}

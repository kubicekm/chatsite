<?php

declare(strict_types=1);

namespace App\Presenters;

abstract class BasePresenter extends \Nette\Application\UI\Presenter{
    public function beforeRender() {
        parent::beforeRender();
        $this->template->promEverywhere = "Base Presenter Works ";
    }

}

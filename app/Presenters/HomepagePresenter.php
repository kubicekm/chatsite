<?php

namespace App\Presenters;

use App\Model\UsersManager;
use App\Model\ChannelsManager;
use App\Model\ChannelUsersManager;
use App\Model\MessagesManager;
use Nette\Application\UI\Form;
use Nette\Application\UI\Presenter;
use Nette\Security\AuthenticationException;


final class HomepagePresenter extends BasePresenter{
    /** @var UsersManager @inject */
    public $usersManager;

    /** @var ChannelsManager @inject */
    public $channelsManager;

    /** @var ChannelUsersManager @inject */
    public $channelUsersManager;

    /** @var MessagesManager @inject */
    public $messagesManager;

    public function createComponentSignUpForm(): Form {
        $form = new Form();
        $form->addText('username', 'Username')
            ->setRequired('Please type in your username.')
            ->addRule($form::MAX_LENGTH, 'Username may only have up to %d characters', 30);
        $form->addEmail('email', 'E-mail')
            ->setRequired('Please type in your password');
        $form->addPassword('password', 'Password')
            ->addRule($form::MIN_LENGTH, 'Password must have at least %d characters', 8)
            ->addRule($form::PATTERN, 'Must contain a number', '.*[0-9].*')
            ->setRequired('You must type in your password!');
        $form->addPassword('password2', 'Repeat password')
            ->setRequired('You must repeat your password!')
            ->addRule($form::EQUAL, 'Passwords must match!', $form['password'])
            ->setOmitted(true);
        $form->addSubmit('send', 'Sign-in');
        $form->onSuccess[] = [$this, 'onSignUpFormSubmit'];
        return $form;
    }

    public function onSignUpFormSubmit(\stdClass $values) {
        if ($this->usersManager->checkIfExists($values->username, $values->email)) {
            $this->flashMessage('Username already in use!', 'danger');
            return;
        }

        $this->usersManager->addUser($values->username, $values->password, $values->email);
        $this->flashMessage('Username registered, you may now sign in!', 'success');
        $this->redirect('default');
    }

    public function createComponentCreateChannelForm(): Form {
        $form = new Form();
        $form->addText('name', 'Channel name')
            ->setRequired('Choose a name for your channel.')
            ->addRule($form::MAX_LENGTH, 'Channel name may only have up to %d characters', 30);
        $form->addSubmit('send', 'Create channel');
        $form->onSuccess[] = [$this, 'onCreateChannelFormSubmit'];
        return $form;
    }

    public function onCreateChannelFormSubmit(\stdClass $values) {
        $col = $this->channelsManager->addChannel($values->name);
        $this->channelUsersManager->addChannelUsers($col->id,$this->getUser()->id);
        $this->flashMessage('Channel created!', 'success');
        $this->redirect('default');
    }

    public function createComponentSendMessageForm(): Form {
        $form = new Form();
        $form->addTextArea('text', 'Your Message')
            ->setRequired('Please type in a message!.')
            ->addRule($form::MAX_LENGTH, 'Message is too long! Maximum characters allowed is %d.', 1024);
        $form->addSubmit('send', 'Send message');
        $form->onSuccess[] = [$this, 'onSendMessageFormSubmit'];
        return $form;
    }

    public function onSendMessageFormSubmit(\stdClass $values) {
        $channelId = $this->template->channelId;
        $channelUser = $this->channelUsersManager->getChannelUserByChannelAndUser($channelId,$this->getUser()->id);
        if($channelUser == null){
            $this->flashMessage('You are not allowed to speak in this channel!', 'danger');
            return;
        }
        $message = $this->messagesManager->addMessage($values->text,$channelUser->id,$channelId);
        $this->redirect('this');
    }




    public function createComponentChangeChannelUsernameForm(): Form {
        $form = new Form();
        $form->addText('user', 'New Username')
            ->addRule($form::MAX_LENGTH, 'Username may only have up to %d characters', 30);
        $form->addSubmit('send', 'Change Username');
        $form->onSuccess[] = [$this, 'onChangeChannelUsernameFormSubmit'];
        return $form;
    }

    public function onChangeChannelUsernameFormSubmit(\stdClass $values) {
        if ($this->channelUsersManager->checkIfExistsUserWithName($values->user)||$this->usersManager->checkIfExists($values->user,"")){
            $this->flashMessage('Username already in use!', 'danger');
            return;
        }
        $channelId = $this->template->channelId;
        $query = $this->channelUsersManager->changeUsername($this->channelUsersManager->getChannelUserByChannelAndUser($channelId,$this->getUser()->id)->id,$values->user=="" ? null:$values->user);
        $this->redirect('this');
    }



    public function createComponentAddChannelUserForm(): Form {
        $form = new Form();
        $form->addText('user', 'Username')
            ->addRule($form::MAX_LENGTH, 'Username may only have up to %d characters', 30);
        $form->addSubmit('send', 'Add User To Channel');
        $form->onSuccess[] = [$this, 'onAddChannelUserFormSubmit'];
        return $form;
    }

    public function onAddChannelUserFormSubmit(\stdClass $values) {
        if ($this->usersManager->checkIfExists($values->user,"")) {
            if($this->channelUsersManager->checkIfUserIsInChannel($this->usersManager->getUserByUsername($values->user)->id,$this->channelsManager->getChannel($this->template->channelId)->id)){
                $this->flashMessage('User is already in this channel!', 'danger');
            }else $this->channelUsersManager->addChannelUsers($this->channelsManager->getChannel($this->template->channelId)->id,$this->usersManager->getUserByUsername($values->user)->id);
        }else $this->flashMessage('User with this name does not exist!', 'danger');
        $this->redirect('this');
    }




    public function actionOut() {
        $this->getUser()->logout(true);
        $this->redirect('Homepage:');
    } 

    public function getChannelMessages(int $channelId): ?array{
        $channelMessages = $this->messagesManager->getMessagesByChannel($channelId);
        $realMessages = [];
        foreach($channelMessages as $message){
            $realMessages[]=[
                'id' => $message->id,
                'channel_id' => $channelId,
                'channel_username' => ($this->channelUsersManager->getChannelUser($this->messagesManager->getMessage($message->id)->channel_user)->channel_username==null 
                ?
                 $this->usersManager->getUser($this->channelUsersManager->getChannelUser($this->messagesManager->getMessage($message->id)->channel_user)->user_id)->username 
                :
                $this->channelUsersManager->getChannelUser($this->messagesManager->getMessage($message->id)->channel_user)->channel_username),
                'text' =>  $this->messagesManager->getMessage($message->id)->text,
                'date' => $this->messagesManager->getMessage($message->id)->date,
                'removed' => $this->messagesManager->getMessage($message->id)->removed
            ];
        }
        return $realMessages;
    }

    public function getChannelsOfLoggedUser(): ?array{
        $channelUsers = $this->channelUsersManager->getChannelsByUser($this->getUser()->id);
        $realChannels = [];
        foreach($channelUsers as $channel){
            $realChannels[]=[
                'id' => $channel->id,
                'channel_id' => $channel->channel_id,
                'name' =>  $this->channelsManager->getChannel($channel->channel_id)->name,
                'creation_date' => $this->channelsManager->getChannel($channel->channel_id)->creation_date,
                'removed' => $this->channelsManager->getChannel($channel->channel_id)->removed
            ];
        }
        return $realChannels;
    }

    public function getUsersList(string $startsWith){
        $usersList = $this->usersManager->getUsersNameStartsWith($startsWith,$this->getUser()->id);
    }

    public function actionDefault(int $channelId = -2) { //-2 = homepage, tudíž není otevřený žádný channel, -1 = formulář pro vytvoření nového channelu
        $this->template->channelId = $channelId;
        $this->template->user = $this->getUser();
        $this->template->notAllowed = false;
        if($this->getUser()->isLoggedIn()){
            $this->template->channels = $this->getChannelsOfLoggedUser();
            if($channelId > -1){
                if($this->channelUsersManager->checkIfUserIsInChannel($this->getUser()->id,$channelId)){
                    $this->template->messages = $this->getChannelMessages($channelId);
                }else {
                    $this->template->notAllowed = true;
                    $this->flashMessage('Nice Try!', 'danger');
                }
            }
        }
    }
}

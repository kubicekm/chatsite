<?php

namespace App\Model;

use Nette\Database\Explorer;
use Nette\Database\Table\ActiveRow;

class ChannelsManager{
    const TABLE_NAME = "channels";
    const CHANNEL_ID = "id";
    const CHANNEL = "name";
    const DATE = "creation_date";
    private $explorer;

    public function __construct(\Nette\Database\Explorer $explorer) {
        $this->explorer = $explorer;
    }

    public function addChannel(string $channel): ?ActiveRow {
        return $this->explorer->table(self::TABLE_NAME)
            ->insert([
                self::CHANNEL => $channel
            ]);
    }

    public function removeChannel(int $id) {
        $this->explorer->table(self::TABLE_NAME)->where([self::CHANNEL_ID => $id])->delete();
    }

    public function getChannel(int $id): ?ActiveRow {
        return $this->explorer->table(self::TABLE_NAME)
            ->where([self::CHANNEL_ID => $id])
            ->fetch();
    }

    public function getAllChannels(): ?ActiveRow {
        return $this->explorer->table(self::TABLE_NAME)->fetchAll();
    }

}
<?php

namespace App\Model;

use Nette\Database\Explorer;
use Nette\Database\Table\ActiveRow;

class ChannelUsersManager{
    const TABLE_NAME = "channel_users";
    const CHANNEL_USER = "id";
    const CHANNEL_ID = "channel_id";
    const USER_ID = "user_id";
    const CHANNEL_USERNAME = "channel_username";

    private $explorer;

    public function __construct(\Nette\Database\Explorer $explorer) {
        $this->explorer = $explorer;
    }

    public function addChannelUsers(int $channel, int $id): ?ActiveRow {
        return $this->explorer->table(self::TABLE_NAME)
            ->insert(
                [
                    self::CHANNEL_ID => $channel,
                    self::USER_ID => $id
                ]
            );
    }

    public function deleteChannelUsersByUser(){
        $this->explorer->table(self::TABLE_NAME)->where(self::USER_ID,$id)->delete();
    }

    public function getChannelsByUser(int $id): ?array {
        return $this->explorer->table(self::TABLE_NAME)->where(self::USER_ID,$id)->fetchAll();
    }

    public function getChannelUser(int $id): ?ActiveRow{
        return $this->explorer->table(self::TABLE_NAME)->where(self::CHANNEL_USER,$id)->fetch();
    }

    public function getChannelUserByChannelAndUser(int $channel_id, int $user_id){
        return $this->explorer->table(self::TABLE_NAME)->where(self::CHANNEL_ID,$channel_id)->where(self::USER_ID,$user_id)->fetch();
    }

    public function changeUsername(int $id, string $username = null){
        return $this->explorer->table(self::TABLE_NAME)->where(self::CHANNEL_USER,$id)->update([self::CHANNEL_USERNAME => $username]);
    }

    public function checkIfExistsUserWithName(string $name){
        return $this->explorer->table(self::TABLE_NAME)
            ->where(self::CHANNEL_USERNAME,$name)
            ->count() > 0;
    }

    public function checkIfUserIsInChannel(int $user_id, int $channel_id){
        return $this->explorer->table(self::TABLE_NAME)->where(self::USER_ID,$user_id)->where(self::CHANNEL_ID,$channel_id)->count()>0;
    }


} 
<?php

namespace App\Model;

use Nette\Database\Explorer;
use Nette\Database\Table\ActiveRow;

class MessagesManager{
    const TABLE_NAME = "messages";
    const MESSAGE_ID = "id";
    const MESSAGE = "text";
    const CHANNEL_ID = "channel_id";
    const CHANNEL_USER = "channel_user";
    const DATE = "date";
    private $explorer;

    public function __construct(\Nette\Database\Explorer $explorer) {
        $this->explorer = $explorer;
    }

    public function addMessage(string $message, int $channelUser, int $channelId): ?ActiveRow {
        return $this->explorer->table(self::TABLE_NAME)
            ->insert([
                self::MESSAGE => $message,
                self::CHANNEL_ID => $channelId,
                self::CHANNEL_USER => $channelUser
            ]);
    }

    public function getMessage($id): ?ActiveRow{
        return $this->explorer->table(self::TABLE_NAME)->where([self::MESSAGE_ID => $id])->fetch();
    }

    public function deleteMessagesByUser(int $userId){
        $this->explorer->table(self::TABLE_NAME)->where([self::CHANNEL_USER => $userId])->delete();
    }

    public function deleteMessagesByChannelAndUser(int $channelId, int $userId){
            $this->explorer->table(self::TABLE_NAME)->where([self::CHANNEL_ID => $channelId])->where([self::CHANNEL_USER => $userId])->delete();
        }

    public function getMessagesByChannelAndUser(int $channelId, int $userId): array{
        return $this->explorer->table(self::TABLE_NAME)->where([self::CHANNEL_ID => $channelId])->where([self::CHANNEL_USER => $userId])->fetchAll();
    }

    public function getMessagesByChannel(int $channelId): array{
        bdump($channelId);
        return $this->explorer->table(self::TABLE_NAME)->where([self::CHANNEL_ID => $channelId])->order(self::DATE.' DESC')->fetchAll();
    }

}
<?php

namespace App\Model;

use Nette;
use Nette\Database\Explorer;
use Nette\Database\Table\ActiveRow;
use Nette\Security\AuthenticationException;
use Nette\Security\Authenticator;
use Nette\Security\IIdentity;
use Nette\Security\Passwords;
use Nette\Security\SimpleIdentity;

class UsersManager implements Authenticator {
    const TABLE_NAME = "users";
    const USER_ID = "id";
    const USER = "username";
    const EMAIL = "email";
    const PASS = "password";
    const ROLE = "role";
    private $explorer;
    private $passwords;

    public function __construct(Nette\Database\Explorer $explorer, Nette\Security\Passwords $passwords) {
        $this->explorer = $explorer;
        $this->passwords = $passwords;
    }

    public function authenticate(string $username, string $password): Nette\Security\IIdentity	{
		$row = $this->explorer->table(self::TABLE_NAME)
			->where(self::USER, $username)
			->fetch();

		if (!$row) {
			throw new Nette\Security\AuthenticationException('User not found.');
		}

		if (!$this->passwords->verify($password, $row->password)) {
			throw new Nette\Security\AuthenticationException('Invalid password.');
		}

		return new Nette\Security\Identity($row->id,$row->role,['name' => $row->username]);
	}
    public function addUser(string $username, string $password, string $email, int $role = 0): ?ActiveRow {
        return $this->explorer->table(self::TABLE_NAME)
            ->insert([
                self::USER => $username,
                self::PASS => $this->passwords->hash($password),
                self::EMAIL => $email,
                self::ROLE => $role
            ]);
    }

    public function removeUser(int $id) {
        $this->explorer->table(self::TABLE_NAME)->where([self::USER_ID => $id])->delete();
    }

    public function checkIfExists(string $name, string $email): bool {
        return $this->explorer->table(self::TABLE_NAME)
            ->where([self::USER. ' = ? OR ' . self::EMAIL . '= ?' => [$name, $email]])
            ->count() > 0;
    }

    public function getUser(int $id): ActiveRow {
        return $this->explorer->table(self::TABLE_NAME)
            ->where([UsersManager::USER_ID => $id])
            ->fetch();
    }

    public function getUserByUsername(string $username): ?ActiveRow{
        return $this->explorer->table(self::TABLE_NAME)->where([UsersManager::USER => $username])->fetch();
    }

    public function getUsersNameStartsWith(string $startString, int $excludedUser = 0): ?array{
        return $this->explorer->table(self::TABLE_NAME)->where('username LIKE ?', 'startString')->where('id NOT IN','excludedUser')->fetchAll();
    }

    public function getAllUsers(): array {
        return $this->explorer->table(self::TABLE_NAME)->fetchAll();
    }

}